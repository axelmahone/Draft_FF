﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Draft_FF.Frontend.DTOs
{
    public class DraftPickInfoDto
    {
        public DraftSlotDto CurrentDraftSlot { get; set; }
        public PlayerDto PickedPlayer { get; set; }
    }
}
