﻿using Draft_FF.Frontend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Draft_FF.Frontend.DTOs
{
    public class DraftSlotDto
    {
        public int Id { get; set; }
        public int? SelectedPlayerId { get; set; }
        public int OwnerId { get; set; }

        public int Round { get; set; }
        public int RankInRound { get; set; }
        public int Overall { get; set; }

        public string PickedPlayerName { get; set; }
    }
}