﻿using Draft_FF.Frontend.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Draft_FF.Frontend.DTOs
{
    public class OwnerDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string TeamName { get; set; }

        public string ShortName { get; set; }

        public int EspnId { get; set; }

        public bool IsNewTeam { get; set; }
    }
}
