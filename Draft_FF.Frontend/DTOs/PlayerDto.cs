﻿namespace Draft_FF.Frontend.DTOs
{
    public class PlayerDto
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Position { get; set; }

        public string ImageAsBase64 { get; set; }

        public int TeamId { get; set; }
        public string TeamShortName { get; set; }

        public int? OwnerId { get; set; }

        public double ProjectedPoints { get; set; }

        public int? Rank { get; set; }

        public int SelectedInDraftSlotId { get; set; }

        public bool IsDrafted { get; set; }
    }
}
