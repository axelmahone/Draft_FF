﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Draft_FF.Frontend.DTOs
{
    public class PickDto
    {
        public int PlayerId { get; set; }
        public int PickSlotId { get; set; }
    }
}
