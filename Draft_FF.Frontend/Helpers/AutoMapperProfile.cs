﻿namespace Draft_FF.Frontend.Helpers
{
    using AutoMapper;
    using Draft_FF.Frontend.Entities;
    using Draft_FF.Frontend.DTOs;

    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<Owner, OwnerDto>();
            CreateMap<OwnerDto, Owner>();

            CreateMap<DraftPickSlot, DraftSlotDto>();

            CreateMap<Player, PlayerDto>().ForMember(dest => dest.TeamShortName, opt => opt.MapFrom(src => src.Team.ShortName)).ReverseMap();
        }
    }
}
