﻿namespace Draft_FF.Frontend.Helpers
{
    public class AppSettings
    {
        public string DbConnectionString { get; set; }
        public string JwtSecret { get; set; }
    }
}
