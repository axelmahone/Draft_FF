﻿using Draft_FF.Frontend.DTOs;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Draft_FF.Frontend.Helpers
{
    public class DraftHub : Hub
    {
        public DraftHub()
        {

        }

        public void NotifyOwners(DraftPickInfoDto draftPickInfo)
        {
            Clients.All.SendAsync("PlayerSelected", draftPickInfo);
        }
    }
}
