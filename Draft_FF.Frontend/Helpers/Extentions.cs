﻿namespace Draft_FF.Frontend.Helpers
{
    using Draft_FF.Frontend.Entities;
    using Microsoft.EntityFrameworkCore;
    using System;
	using System.Collections.Generic;
    using System.Linq;
    using System.Text;
	
    public static class Extentions
    {
        public static Dictionary<int, T> ToDictionary<T>(this IQueryable<T> argData) where T : CDbEntity
        {
            return argData.ToDictionary(e => e.Id, e => e);
        }

        public static string ToString(this Owner argOwner)
        {
            return $"{argOwner.Name} ({argOwner.Id})";
        }

        public static string ToString(this DraftPickSlot argPickSlot)
        {
            return $"{nameof(argPickSlot.Round)}: {argPickSlot.Round}, {nameof(argPickSlot.RankInRound)}: {argPickSlot.RankInRound}, {nameof(argPickSlot.Overall)} {argPickSlot.Overall}" + $"{(argPickSlot.OwnerId.HasValue ? $"{nameof(argPickSlot.Owner)}: {argPickSlot.Owner}" : "")}";
        }
    }
}
