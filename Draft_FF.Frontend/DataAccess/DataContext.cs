﻿namespace Draft_FF.Frontend.DataAccess
{
    using Draft_FF.Frontend.Entities;
    using Microsoft.EntityFrameworkCore;
    using System;

    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> argOptions)
            : base(argOptions)
        {

        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Draft> Drafts { get; set; }
        public DbSet<DraftPickSlot> DraftPickSlots { get; set; }
                
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connectionString = @"Data Source=(local)\sqlserver2016;Initial Catalog=Draft_FF2;User ID=sa;Password=P@ssw0rd;Persist Security Info=True";
            optionsBuilder.UseSqlServer(connectionString);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Player>().Property(e => e.Position).HasConversion<string>();

            modelBuilder.Entity<Player>().HasIndex(e => e.EspnId).IsUnique();
            modelBuilder.Entity<Team>().HasIndex(e => e.EspnId).IsUnique();
            modelBuilder.Entity<Team>().HasIndex(e => e.ShortName).IsUnique();
            modelBuilder.Entity<Team>().HasIndex(e => e.Name).IsUnique();

            modelBuilder.Entity<Player>().HasOne(e => e.SelectedInDraftSlot).WithOne(m => m.SelectedPlayer);
            modelBuilder.Entity<DraftPickSlot>().HasOne(e => e.Owner).WithMany(m => m.DraftPickSlots);

            modelBuilder.Entity<Owner>().HasData(
                new Owner { Id = 1, EspnId = 1, Name = "Simon W", ShortName = "TMW", TeamName = "Team Willi" },
                new Owner { Id = 2, EspnId = 2, Name = "Mathias Schmid", ShortName = "LEAP", TeamName = "Lambeau Leaper" },
                new Owner { Id = 3, EspnId = 3, Name = "Jakob Schallert", ShortName = "WIN", TeamName = "All I do is Winston" },
                new Owner { Id = 4, EspnId = 4, Name = "Valentin Moosmann", ShortName = "STIG", TeamName = "Stigling Ballers" },
                new Owner { Id = 5, EspnId = 5, Name = "Jonathan De Greve", ShortName = "AfB", TeamName = "Alvin and the KareemHunts" },
                new Owner { Id = 6, EspnId = 6, Name = "Dominik S", ShortName = "AKH", TeamName = "Team Willi" },
                new Owner { Id = 7, EspnId = 7, Name = "Daniel Puschnigg", ShortName = "PUSC", TeamName = "Team Puschnigg" },
                new Owner { Id = 8, EspnId = 10, Name = "Benjamin Puschnigg", ShortName = "CM", TeamName = "Hard CM" },
                new Owner { Id = 9, EspnId = 11, Name = "Luke Vlbg", ShortName = "VLBG", TeamName = "Team Vlbg" },
                new Owner { Id = 10, EspnId = 12, Name = "Buechele Juloan", ShortName = "JULI", TeamName = "SF 49er" },
                new Owner { Id = 11, EspnId = 13, Name = "Lukas G", ShortName = "G", TeamName = "Team G" },
                new Owner { Id = 12, EspnId = 14, Name = "Thomas Fink", ShortName = "FINK", TeamName = "Thomas Fink" }
                );

            modelBuilder.Entity<Team>().HasData(
                new Team { Id = Constants.FA_TEAM_ID, EspnId = 0, Name = "FA", ByeWeek = 0,  ShortName = "FA", IsRealTeam = false },
                new Team { Id = 2, EspnId = 1, Name = "Atlanta Falcons", ByeWeek = 8,  ShortName = "Atl", IsRealTeam = true },
                new Team { Id = 3, EspnId = 2, Name = "Buffalo Bills", ByeWeek = 11, ShortName = "Buf", IsRealTeam = true },
                new Team { Id = 4, EspnId = 3, Name = "Chicago Bears", ByeWeek = 5, ShortName = "Chi", IsRealTeam = true },
                new Team { Id = 5, EspnId = 4, Name = "Cincinnati Bengals", ByeWeek = 9, ShortName = "Cin", IsRealTeam = true },
                new Team { Id = 6, EspnId = 5, Name = "Cleveland Browns", ByeWeek = 11, ShortName = "Cle", IsRealTeam = true },
                new Team { Id = 7, EspnId = 6, Name = "Dallas Cowboys", ByeWeek = 8, ShortName = "Dal", IsRealTeam = true },
                new Team { Id = 8, EspnId = 7, Name = "Denver Broncos", ByeWeek = 10, ShortName = "Den", IsRealTeam = true },
                new Team { Id = 9, EspnId = 8, Name = "Detroit Lions", ByeWeek = 6, ShortName = "Det", IsRealTeam = true },
                new Team { Id = 10, EspnId = 9, Name = "Green Bay Packers", ByeWeek = 7, ShortName = "GB", IsRealTeam = true },
                new Team { Id = 11, EspnId = 10, Name = "Tennessee Titans", ByeWeek = 8, ShortName = "Ten", IsRealTeam = true },
                new Team { Id = 12, EspnId = 11, Name = "Indianapolis Colts", ByeWeek = 9, ShortName = "Ind", IsRealTeam = true },
                new Team { Id = 13, EspnId = 12, Name = "Kansas City Chiefs", ByeWeek = 12, ShortName = "KC", IsRealTeam = true },
                new Team { Id = 14, EspnId = 13, Name = "Oakland Raiders", ByeWeek = 7, ShortName = "Oak", IsRealTeam = true },
                new Team { Id = 15, EspnId = 14, Name = "Los Angeles Rams", ByeWeek = 12, ShortName = "LAR", IsRealTeam = true },
                new Team { Id = 16, EspnId = 15, Name = "Miami Dolphins", ByeWeek = 11, ShortName = "Mia", IsRealTeam = true },
                new Team { Id = 17, EspnId = 16, Name = "Minnesota Vikings", ByeWeek = 10, ShortName = "Min", IsRealTeam = true },
                new Team { Id = 18, EspnId = 17, Name = "New England Patriots", ByeWeek = 11, ShortName = "NE", IsRealTeam = true },
                new Team { Id = 19, EspnId = 18, Name = "New Orleans Saints", ByeWeek = 6, ShortName = "NO", IsRealTeam = true },
                new Team { Id = 20, EspnId = 19, Name = "New York Giants", ByeWeek = 9, ShortName = "NYG", IsRealTeam = true },
                new Team { Id = 21, EspnId = 20, Name = "New York Jets", ByeWeek = 11, ShortName = "NYJ", IsRealTeam = true },
                new Team { Id = 22, EspnId = 21, Name = "Philadelphia Eagles", ByeWeek = 9, ShortName = "Phi", IsRealTeam = true },
                new Team { Id = 23, EspnId = 22, Name = "Arizona Cardinals", ByeWeek = 9, ShortName = "Ari", IsRealTeam = true },
                new Team { Id = 24, EspnId = 23, Name = "Pittsburgh Steelers", ByeWeek = 7, ShortName = "Pit", IsRealTeam = true },
                new Team { Id = 25, EspnId = 24, Name = "Los Angeles Chargers", ByeWeek = 8, ShortName = "LAC", IsRealTeam = true },
                new Team { Id = 26, EspnId = 25, Name = "San Francisco 49ers", ByeWeek = 11, ShortName = "SF", IsRealTeam = true },
                new Team { Id = 27, EspnId = 26, Name = "Seattle Seahawks", ByeWeek = 7, ShortName = "Sea", IsRealTeam = true },
                new Team { Id = 28, EspnId = 27, Name = "Tampa Bay Buccaneers", ByeWeek = 5, ShortName = "TB", IsRealTeam = true },
                new Team { Id = 29, EspnId = 28, Name = "Washington Redskins", ByeWeek = 4, ShortName = "Wsh", IsRealTeam = true },
                new Team { Id = 30, EspnId = 29, Name = "Carolina Panthers", ByeWeek = 4, ShortName = "Car", IsRealTeam = true },
                new Team { Id = 31, EspnId = 30, Name = "Jacksonville Jaguars", ByeWeek = 9, ShortName = "Jax", IsRealTeam = true },
                new Team { Id = 32, EspnId = 33, Name = "Baltimore Ravens", ByeWeek = 10, ShortName = "Bal", IsRealTeam = true },
                new Team { Id = 33, EspnId = 34, Name = "Houston Texans", ByeWeek = 10, ShortName = "Hou", IsRealTeam = true }
                );
            
            base.OnModelCreating(modelBuilder);
        }
    }
}



































