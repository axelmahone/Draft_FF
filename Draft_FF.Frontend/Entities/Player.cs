﻿namespace Draft_FF.Frontend.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Player : CDbEntity
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Required]
        public Enums.Position Position { get; set; }

        public string ImageAsBase64 { get; set; }

        [Required]
        public int TeamId { get; set; }
        [Required]
        public Team Team { get; set; }

        public int? OwnerId { get; set; }
        public virtual Owner Owner { get; set; }

        [Required]
        public int EspnId { get; set; }

        public int? EspnImageNumber { get; set; }

        public double ProjectedPoints { get; set; }

        public int? Rank { get; set; }

        public DraftPickSlot SelectedInDraftSlot { get; set; }

        public bool GetIsDrafted()
        {
            return SelectedInDraftSlot != null;
        }
    }
}