﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Draft_FF.Frontend.Entities
{
    public class Enums
    {
        public enum Position
        {
            QB = 1,
            RB = 2,
            WR = 3,
            TE = 4,
            K = 5,
            DST = 6,
        }
    }
}
