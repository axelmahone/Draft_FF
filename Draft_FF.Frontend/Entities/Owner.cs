﻿namespace Draft_FF.Frontend.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class Owner : CDbEntity
    {
        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        [Required]
        [MaxLength(50)]
        public string TeamName { get; set; }

        [Required]
        [MaxLength(4)]
        public string ShortName { get; set; }

        public ICollection<Player> Players { get; set; }

        [Required]
        public int EspnId { get; set; }

        public ICollection<DraftPickSlot> DraftPickSlots { get; set; }
    }
}