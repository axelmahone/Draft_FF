﻿namespace Draft_FF.Frontend.Entities
{
    using System.ComponentModel.DataAnnotations;

    public class Team : CDbEntity
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string ShortName { get; set; }

        [Required]
        public int ByeWeek { get; set; }

        public int EspnId { get; set; }
        public bool IsRealTeam { get; set; }
    }
}