﻿namespace Draft_FF.Frontend.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public class DraftPickSlot : CDbEntity
    {
        [Required]
        public int Overall { get; set; }
        [Required]
        public int Round { get; set; }
        [Required]
        public int RankInRound { get; set; }

        public string Notice { get; set; }

        public int PlaceHolder { get; set; }

        public int? OwnerId { get; set; }

        public Owner Owner { get; set; }

        public int? SelectedPlayerId { get; set; }

        public virtual Player SelectedPlayer { get; set; }
    }
}