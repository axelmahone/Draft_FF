﻿namespace Draft_FF.Frontend.Entities
{
	using System;
	using System.Collections.Generic;
	using System.Text;

    public class Draft : CDbEntity
    {
        public string DraftName { get; set; }
        public ICollection<DraftPickSlot> Slots { get; set; }
    }
}
