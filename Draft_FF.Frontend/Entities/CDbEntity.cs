﻿namespace Draft_FF.Frontend.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    public abstract class CDbEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
    }
}
