﻿const playerPickType = 'PLAYER_PICKED';
const ownerSelectedType = 'OWNER_SELECTED';

const initialState = {
    session: {
        ownerId: null
    },
    currentPickNumber: 1,
    pickableOwnerId: '',
    owners: [],
    slots: [],
    players: []
}

export const actionCreators = {
    playerPick: () => ({ type: playerPickType }),
    ownerSelected: (argOwnerId) => ( {type: ownerSelectedType, ownerId: argOwnerId })
};

export const reducer = (currentState = initialState, action) => {
    let newState = {
        ...currentState
    }

    switch(action.type) {

        case ownerSelectedType:
       
            newState.session.ownerId = action.owner.id;
            newState.session.ownerName = action.owner.name;

            return newState;

        case playerPickType:

            var changedPlayer = action.changedPlayer;

            return newState.players.map((player) => {
                
                if (player.id === changedPlayer.id) {
                    player.properties = { ...changedPlayer.properties };
                }

                return player
            });

    }
    
    return { ...currentState };
};