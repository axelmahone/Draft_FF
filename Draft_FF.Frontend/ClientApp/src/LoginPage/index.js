import React, { Component } from 'react'
import { SplitButton, MenuItem } from 'react-bootstrap';
import { connect } from 'react-redux'
 
import { ownerActions } from '../_actions'

class LoginPage extends Component {

    constructor(props) {
        super(props)

        this.props.dispatch(ownerActions.logout())
        this.props.dispatch(ownerActions.getAll())

        this.state = {
            ownerId: 0,
            submitted: false
        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    handleChange(selectedOwnerId, e) {
        e.preventDefault()

        this.setState({ ownerId: selectedOwnerId });
    }

    handleSubmit(e) {
        e.preventDefault()

        this.setState( {submitted: true } )
        const { ownerId } = this.state
        const { dispatch } = this.props

        if (ownerId > 0) {
            dispatch(ownerActions.login(ownerId))
        }
    }

    render() {
        const { owners } = this.props
        const { ownerId, submitted} = this.state

        var title = 'select an owner'

        if (owners.items) {
            var selectedOwner = owners.items.filter(e => e.id === ownerId);

            if (selectedOwner.length === 1) {
                title = selectedOwner[0].name
            }
        }


        return (
            <div className='col-md-6 col-md-offset-3'>
                <h2>Login</h2>
                <form name='form' onSubmit={this.handleSubmit}>
                    <div className={'form-group' + (submitted && !ownerId ? ' has-error' : '')}>
                        {owners.loading && <em>Loading owners</em>}
                        {owners.items &&
                            <SplitButton title={title} id='ownerId'>
                                {owners.items.map(owner => 
                                     <MenuItem eventKey={owner.id} key={owner.id} onSelect={this.handleChange}>{owner.name}</MenuItem>
                                )}
                            </SplitButton>
                        }
                        &nbsp;
                        <button className="btn btn-primary">Login</button>
                    </div>
                </form>
            </div>
        )
    }
}


function mapStateToProps(state) {
    const { owners } = state;
    const { loggingIn } = state.authentication;
    return {
        loggingIn,
        owners,
    };
}
 
const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage };