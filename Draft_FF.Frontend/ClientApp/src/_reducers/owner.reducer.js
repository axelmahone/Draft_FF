import { ownerConstants } from '../_constants'

export const owners = (state = {}, action) => {
    switch (action.type) {
        case ownerConstants.GETALL_REQUEST:
          return {
            loading: true
          };
        case ownerConstants.GETALL_SUCCESS:
          return {
            items: action.owners
          };
        case ownerConstants.GETALL_FAILURE:
          return { 
            error: action.error
          };
        default:
          return state
      }
}
