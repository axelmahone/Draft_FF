import { combineReducers } from 'redux'

import { authentication } from './authentication.reducer'
import { owners } from './owner.reducer'
import { players } from './player.reducer'
import { draft } from './draft.reducer'

const rootReducer = combineReducers({
  authentication,
  owners,
  players,
  draft
});

export default rootReducer;