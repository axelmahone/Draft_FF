import { playerConstants } from '../_constants'

export const players = (state = {}, action) => {
    switch (action.type) {
        case playerConstants.GETALL_REQUEST:
          return {
            loading: true
          };
        case playerConstants.GETALL_SUCCESS:
          var playerDict = []

          action.players.forEach(item => 
            playerDict[item.id] = item
          )

          return {
            ...state,
            loading: false,
            items: playerDict
          };
        case playerConstants.GETALL_FAILURE:
          return { 
            error: action.error
          };
        
        case playerConstants.GET_AVAILABLE:
          if (state.loading)
            return state

          let { isNewTeam, pickOwnerId, ownOwnerId, currentRound } = action

          let availablePlayers = []

          availablePlayers = state.items.filter(player => !player.isDrafted)

          if (currentRound < 13) {
            if(isNewTeam) {
              availablePlayers = availablePlayers.filter(player => player.ownerId === null || player.ownerId === pickOwnerId)
            } else {
              availablePlayers = availablePlayers.filter(player => player.ownerId === null || player.ownerId === ownOwnerId)
            }
          }

          availablePlayers.sort((a, b) => {
            if (a.ownerId === ownOwnerId && b.ownerId === ownOwnerId) {
              return b.projectedPoints - a.projectedPoints
            }    
            
            if (a.ownerId === ownOwnerId)
              return -1

            return 1

          })

          return { 
            ...state,
            availablePlayers: availablePlayers
          }

        case playerConstants.PLAYERS_UPDATE:
          var newPlayers = {...state}

          if(action.players) {
            action.players.forEach(player => {
              newPlayers.items[player.id] = player
            });
          }

          return newPlayers
        
        default:
          return state
      }
}
