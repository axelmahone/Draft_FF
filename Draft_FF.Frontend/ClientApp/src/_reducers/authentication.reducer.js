import { ownerConstants } from '../_constants';
 
let owner = JSON.parse(localStorage.getItem('owner'));
const initialState = owner ? { loggedIn: true, owner } : {};
 
export const authentication = (state = initialState, action) => {
  switch (action.type) {
    case ownerConstants.LOGIN_REQUEST:
      return {
        loggingIn: true,
        user: action.user
      };
    case ownerConstants.LOGIN_SUCCESS:
      return {
        loggedIn: true,
        user: action.user
      };
    case ownerConstants.LOGIN_FAILURE:
      return {};
    case ownerConstants.LOGOUT:
      return {};
    default:
      return state
  }
}