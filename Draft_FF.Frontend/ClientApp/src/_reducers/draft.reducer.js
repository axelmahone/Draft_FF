import { draftConstants } from '../_constants';
  
export const draft = (state = {}, action) => {
  switch (action.type) {
    case draftConstants.GETALL_DRAFTSLOTS_REQUEST:
        return {
            loading: true
        };
    case draftConstants.GETALL_DRAFTSLOTS_SUCCESS:
        return {
            items: action.draftSlots
        };
        case draftConstants.GETALL_DRAFTSLOTS_F:
            return { 
                error: action.error
            };
    case draftConstants.SET_CURRENT_PICK:
        return { ...state, currentPick: action.currentPick };
    default:
      return state
  }
}