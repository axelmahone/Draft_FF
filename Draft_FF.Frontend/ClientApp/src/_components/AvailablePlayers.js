import React from 'react'
import PropTypes from 'prop-types'
import { Badge } from 'react-bootstrap'

export const AvailablePlayers = ({ players, handlePick, isAllowedToPick, ownerId }) => {

    if (players.loading) {
        return (<span>loading</span>)
    }

    var availablePlayers = players.availablePlayers || players.items

    return(
        <ul id='available-players' style={{ width: '300px'}}>
            {availablePlayers &&
                availablePlayers.map(player =>
                    <li key={player.id} style={{display: 'block', height: '28px'}}>
                        <span style={{fontWeight: ownerId === player.ownerId ? 'bold' : 'normal' }}>
                            {player.name}
                        </span>, {player.teamShortName} &nbsp;
                        <Badge className={'badge-position-' + player.position}>{player.position}</Badge>
                        {isAllowedToPick && <button style={{float: 'right'}} onClick={() => handlePick(player)} data-player={player}>Draft</button>}
                        
                    </li>
                )
            }
        </ul>
    )
}


AvailablePlayers.propTypes = {
    players: PropTypes.object.isRequired,
    handlePick: PropTypes.func.isRequired,
    isAllowedToPick: PropTypes.bool.isRequired,
}
