import React from 'react'
import PropTypes from 'prop-types'

export const DraftStatus = ({overall, round, rankInRound}) => 
    <div>
        <h3>Overall: {overall}</h3>
        <h4>Round: {round} Pick: {rankInRound}</h4>
    </div>


DraftStatus.propTypes = {
    overall: PropTypes.number.isRequired,
    round: PropTypes.number.isRequired,
    rankInRound: PropTypes.number.isRequired,
}

