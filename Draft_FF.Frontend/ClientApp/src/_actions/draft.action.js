import { draftConstants } from '../_constants'
import { draftService } from '../_services'

export const draftActions = {
    getAll,
    pick
}

function getAll() {

    const request = () => ({ type: draftConstants.GETALL_DRAFTSLOTS_REQUEST })
    const success = draftSlots => ({ type: draftConstants.GETALL_DRAFTSLOTS_SUCCESS, draftSlots })
    const failure = error => ({ type: draftConstants.GETALL_DRAFTSLOTS_FAILURE, error })

    return dispatch => {
        dispatch(request())

        draftService
            .getAll()
            .then(draftSlots => dispatch(success(draftSlots)))
            .catch(error => dispatch(failure(error.toString())))
    
    }
}

function pick(player) {
    draftService.pick(player)
}

