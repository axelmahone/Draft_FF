import { playerConstants } from '../_constants'
import { playerService } from '../_services'

export const playerActions = {
    getAll,
    getAvailablePlayers,
    updatePlayers
}

function getAll() {

    const request = () => ({ type: playerConstants.GETALL_REQUEST })
    const success = players => ({ type: playerConstants.GETALL_SUCCESS, players })
    const failure = error => ({ type: playerConstants.GETALL_FAILURE, error })

    return dispatch => {
        dispatch(request())

        playerService
            .getAll()
            .then(players => dispatch(success(players)))
            .catch(error => dispatch(failure(error.toString())))
    }
}

function getAvailablePlayers(currentRound, pickOwnerId, owner) {

    return dispatch => {
        dispatch(
            {
                type: playerConstants.GET_AVAILABLE, 
                isNewTeam: owner.isNewTeam,
                ownOwnerId: owner.id,
                currentRound, 
                pickOwnerId})
    }

}

function updatePlayers(players = []) {

    return dispatch => dispatch({type: playerConstants.PLAYERS_UPDATE, players})

}