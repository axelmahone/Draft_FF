import { ownerConstants } from '../_constants'
import { ownerService } from '../_services'
import { history } from '../_helpers'

export const ownerActions = {
    login,
    logout,
    getAll
}

function login(ownerId) {

    const request = ownerId => ({ type: ownerConstants.LOGIN_REQUEST, ownerId })
    const success = ownerId => ({ type: ownerConstants.LOGIN_SUCCESS, ownerId })
    const failure = error => ({ type: ownerConstants.LOGIN_FAILURE, error })

    return dispatch => {
        dispatch(request({ ownerId }))

        ownerService
            .login(ownerId)
            .then(owner => {
                dispatch(success(owner))        
                history.push('/');
            })
            .catch(error => dispatch(failure(error.toString())))

    }
}
function logout() {
    ownerService.logout()
    return { type: ownerConstants.LOGOUT }
}

function getAll() {

    const request = () => ({ type: ownerConstants.GETALL_REQUEST })
    const success = owners => ({ type: ownerConstants.GETALL_SUCCESS, owners })
    const failure = error => ({ type: ownerConstants.GETALL_FAILURE, error })

    return dispatch => {
        dispatch(request())

        ownerService
            .getAll()
            .then(owners => dispatch(success(owners)))
            .catch(error => dispatch(failure(error.toString())))
    
    }
}