import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap';
import { connect } from 'react-redux'

import { webSocketService } from '../_services'
import { playerActions, draftActions } from '../_actions'
import { AvailablePlayers, DraftStatus } from '../_components'


class DraftBoard extends Component {

    constructor(props) {
        super(props)
        
        var o = JSON.parse(localStorage.getItem('owner'));

        this.state = {
            owner: {...o},
        }
        
        this.props.dispatch(playerActions.getAll())
        this.props.dispatch(draftActions.getAll())
        this.props.dispatch(playerActions.getAvailablePlayers(1, 1, this.state.owner))

        this.socket = new webSocketService()
        this.socket.registerPlayerSelected((draftPick) => { console.log(draftPick) })
    }

    handlePick(player) {
        this.props.dispatch(playerActions.getAvailablePlayers(1, 1, this.state.owner))
        draftActions.pick(player)
    }

    render() {
        return (
            <Grid id='draft-board'>
                <Row style={{ height: 100 + 'px' }}>
                    <Col lg={6} style={{ backgroundColor: '#ddd', height: 100 + 'px' }}>
                        <DraftStatus overall={1} round={1} rankInRound={1} />
                        </Col>
                    <Col lg={6} style={{ backgroundColor: '#ddd', height: 100 + 'px' }}>
                        <h1>{this.state.owner.name}</h1>
                    </Col>
                </Row> 
                <Row>
                    <Col lg={3} style={{ backgroundColor: 'rgba(255, 0, 0, 0.1)' }}>
                    </Col>
                    <Col lg={6} style={{ backgroundColor: 'rgba(0, 0, 255, 0.2)' }}>
                        <AvailablePlayers players={this.props.players} isAllowedToPick={true} 
                            handlePick={this.handlePick.bind(this)} ownerId={this.state.owner.id} />
                    </Col>
                    <Col lg={3} style={{ backgroundColor: 'rgba(0, 255, 0, 0.2)' }}></Col>
                </Row>
            </Grid>
        )
    }
}
function mapStateToProps(state) {
    const { players, draft } = state;

    return {
        players,
        draft
    };
}
 
const connectedDraftBoard = connect(mapStateToProps)(DraftBoard);
export { connectedDraftBoard as DraftBoard };