﻿import React, { Component } from 'react'
import { Router, Route } from 'react-router-dom';
import { PrivateRoute } from './_components';
import { connect } from 'react-redux'

import { history } from './_helpers'
import { LoginPage } from './LoginPage'
import { DraftBoard } from './DraftBoard'

class App extends Component {

  render() {
    return(
        <Router history={history}>
          <div>
            <PrivateRoute exact path='/' component={DraftBoard} />
            <Route path='/login' component={LoginPage} />
          </div>
        </Router>
    )
  }
}

function mapStateToProps(state) {
  const { alert } = state
  return {
      alert
  };
}

const connectedApp = connect(mapStateToProps)(App)
export { connectedApp as App }