export const ownerConstants = {
    LOGIN_REQUEST: 'OWNERS_LOGIN_REQUEST',
    LOGIN_SUCCESS: 'OWNERS_LOGIN_SUCCESS',
    LOGIN_FAILURE: 'OWNERS_LOGIN_FAILURE',  

    LOGOUT: 'OWNERS_LOGOUT',

    GETALL_REQUEST: 'OWNERS_GETALL_REQUEST',
    GETALL_SUCCESS: 'OWNERS_GETALL_SUCCESS',
    GETALL_FAILURE: 'OWNERS_GETALL_FAILURE',
}