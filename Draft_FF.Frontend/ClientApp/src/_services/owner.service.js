export const ownerService = {
    login,
    logout,
    getAll,
};

function login(ownerId) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' }
    }

    return fetch(`${"https://localhost:44313"}/api/owner/authenticate/${ownerId}`, requestOptions)
        .then(handleResponse)
        .then(owner => {
            if(owner.token) {
                localStorage.setItem('owner', JSON.stringify(owner));
            }
            return owner;
        })
}

function logout() {
    localStorage.removeItem('owner')
}

function getAll() {
    const requestOptions = {
        method: 'GET'
    }

    return fetch(`${"https://localhost:44313"}/api/owner`, requestOptions)
        .then(handleResponse)
}

function handleResponse(response) {
    return response.text()
        .then(text => {
            const data = text && JSON.parse(text);
            if (!response.ok) {
                if (response.status === 401) {
                    logout();
                    window.location.reload(true);
                }
     
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error)
            }
     
            return data
        })
}