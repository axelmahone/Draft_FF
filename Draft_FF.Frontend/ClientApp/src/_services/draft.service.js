import { authHeader } from '../_helpers';
import { webSocketService } from './websocket.service'

export const draftService = {
    getAll,
    initWebSocket,
    pick
}

function initWebSocket() {
    this.webSocket = new webSocketService()
    this.webSocket.registerPlayerSelected(handlePickReceived)
}

function handlePickReceived(draftPick) {
    console.log(draftPick)
}

function pick(draftPick) {

    const requestOptions = {
        method: 'POST',
        headers: { 
            ...authHeader(), 
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({ PlayerId: draftPick.id, PickSlotId: 1 })
    }

    return fetch(`${"https://localhost:44313"}/api/draft/pick`, requestOptions)
        .then(handleResponse)
        .then(() => true)
        .catch(err => {console.log(err); return false})
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: { ...authHeader() }
    }

    return fetch(`${"https://localhost:44313"}/api/draft/DraftSlots`, requestOptions)
        .then(handleResponse)
}

function handleResponse(response) {
    return response.text()
        .then(text => {
            const data = text && JSON.parse(text);
            if (!response.ok) {
                console.log(response)
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error)
            }
     
            return data
        })
}