export const playerService = {
    getAll,
};

function getAll() {
    return fetch(`${"https://localhost:44313"}/api/players`)
        .then(handleResponse)
}

function handleResponse(response) {
    return response.text()
        .then(text => {
            const data = text && JSON.parse(text);
            if (!response.ok) {
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error)
            }
     
            return data
        })
}