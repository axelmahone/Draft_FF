import { HubConnectionBuilder } from '@aspnet/signalr'

export class webSocketService {
    
    constructor() {
        this.connection = new HubConnectionBuilder().withUrl(`${"https://localhost:44313"}/hubs/draft`).build();
        
        this.connection
            .start().then(() => console.log("connected to hub"))
            .catch(err => console.log(err))
    }

    registerPlayerSelected(playerSelectedCallback) {
        this.connection.on('PlayerSelected', (dto) => {
            
            playerSelectedCallback(dto)
        })
    }
}