﻿namespace Draft_FF.Frontend.Service
{
    using Serilog;
    using System;
	using System.Collections.Generic;
    using System.Net.Http;
    using System.Text;
	
    public class EspnHttpClient
    {
        private readonly ILogger _Logger;

        public EspnHttpClient(ILogger argLogger)
        {
            _Logger = argLogger;
        }

        public string GetContent(string argUrl)
        {
            var timeout = TimeSpan.FromSeconds(30);

            try
            {
                using (var client = new HttpClient())
                {
                    var response = client.GetAsync(argUrl);
                    var success = response.Wait(timeout);

                    if (success == false)
                    {
                        _Logger.Warning($"{nameof(client.GetAsync)} invoke timed out after {timeout.TotalSeconds}.");
                        return "";
                    }

                    return GetHtmlContent(response.Result);
                }
            }
            catch(Exception ex)
            {
                _Logger.Error(ex, $"{nameof(GetContent)} failed.");
            }

            return "";
        }

        private string GetHtmlContent(HttpResponseMessage argResponse)
        {
            var timeout = TimeSpan.FromSeconds(10);
            var htmlTask = argResponse.Content.ReadAsStringAsync();

            var success = htmlTask.Wait(timeout);

            if (success == false)
            {
                _Logger.Warning($"{nameof(HttpContent.ReadAsStringAsync)} invoke timed out after {timeout.TotalSeconds}.");
                return "";
            }

            return htmlTask.Result;
        }
    }
}
