﻿namespace Draft_FF.Frontend.Service
{
    using Draft_FF.Frontend.DataAccess;
    using Draft_FF.Frontend.Entities;
    using Draft_FF.Frontend.Helpers;
    using Microsoft.EntityFrameworkCore;
    using Serilog;
    using Serilog.Core;
    using System;
	using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class DraftService : CDraftService
    {
        private DraftBoard _DraftBoard;
        private readonly Dictionary<int, Owner> _OwnerCache = new Dictionary<int, Owner>();
        private readonly Dictionary<int, Player> _PlayerCache = new Dictionary<int, Player>();

        public DraftService(ILogger argLogger, DataContext argContext)
            : base(argLogger, argContext)
        {
            var draftSlots = DataContext.DraftPickSlots.AsNoTracking().ToList();

            _DraftBoard = new DraftBoard(draftSlots);
            _OwnerCache = DataContext.Owners.AsNoTracking().ToDictionary();
            _PlayerCache = DataContext.Players.AsNoTracking().ToDictionary();
        }

        public int CurrentDraftPickKey => _DraftBoard.CurrentDraftPickKey;

        public bool Pick(int argSlotId, int argPlayerId, int argOwnerId)
        {
            if (!ValidateOwner(argOwnerId)) 
            {
                Logger.Error($"UserCache does not contain {argOwnerId}");
                return false;
            }

            var currentPickSlot = DataContext.DraftPickSlots.Single(e => e.Overall == _DraftBoard.CurrentDraftPickKey);

            if (!ValidateSlotOwner(currentPickSlot, argOwnerId))
            {
                Logger.Error($"CurrentPick <{currentPickSlot}> is not the pick of <{_OwnerCache[argOwnerId]}>.");
                return false;
            }

            currentPickSlot.SelectedPlayerId = argPlayerId;
            SaveChanges();
            UpdatePlayerCache(argPlayerId);

            return true;
        }

        private void SaveChanges()
        {
            try
            {
                DataContext.SaveChanges();
            }
            catch (Exception ex)
            {
                Logger.Error(ex, $"Could not save changes.");
            }
        }

        private void UpdatePlayerCache(int argPlayerId)
        {
            _PlayerCache[argPlayerId] = DataContext.Players.AsNoTracking().Single(e => e.Id == argPlayerId);
        }

        private bool ValidateOwner(int argOwnerId)
        {
            return _OwnerCache.ContainsKey(argOwnerId);
        }

        private bool ValidateSlotOwner(DraftPickSlot argSlot, int argOwnerGuid)
        {
            return argSlot.OwnerId.Equals(argOwnerGuid);
        }
    }
}
