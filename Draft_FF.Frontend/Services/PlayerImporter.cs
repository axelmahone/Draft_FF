﻿using Draft_FF.Frontend.DataAccess;
using Draft_FF.Frontend.Entities;
using HtmlAgilityPack;
using Serilog;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

namespace Draft_FF.Frontend.Service
{
    public class PlayerImporter
    {
        private readonly ILogger _Logger;
        private readonly EspnHttpClient _EspnHttpClient;
        private readonly int _LeagueId = 618103;
        private readonly DataContext _DataContext;

        public PlayerImporter(DataContext argContext, ILogger argLogger)
        {
            _DataContext = argContext;
            _Logger = argLogger ?? throw new ArgumentNullException(nameof(argLogger));
            _EspnHttpClient = new EspnHttpClient(argLogger);
        }

        public bool ImportPlayerOwnerMapping(int argOwnerId)
        {
            var dbOwner = _DataContext.Owners.Find(argOwnerId);
            
            if (dbOwner == null)
            {
                _Logger.Warning($"No owner in database with {nameof(Owner.Id)} of <{dbOwner}>");
                return false;
            }

            var url = $"http://games.espn.com/ffl/clubhouse?leagueId={_LeagueId}&teamId={dbOwner.EspnId}";

            var html = _EspnHttpClient.GetContent(url);

            var htmlDoc = new HtmlDocument();
            htmlDoc.OptionFixNestedTags = true;
            htmlDoc.LoadHtml(html);

            var rootNode = htmlDoc.DocumentNode;

            var playerIds = MapPlayers(rootNode, argOwnerId);

            var playersToUpdate = _DataContext.Players.Where(e => playerIds.Contains(e.EspnId));

            foreach(var player in playersToUpdate)
            {
                player.OwnerId = argOwnerId;
            }

            _DataContext.SaveChanges();

            return true;
        }
                
        public bool ImportSingleTeam(int argTeamId)
        {
            var dbTeam = _DataContext.Teams.Find(argTeamId);

            if (dbTeam == null)
            {
                _Logger.Warning($"No team in database with {nameof(Team.Id)} of <{argTeamId}>");
                return false;
            }

            var nextUrl = $"http://games.espn.com/ffl/tools/projections?display=alt&leagueId={_LeagueId}&proTeamId={dbTeam.EspnId}";

            var hasMorePages = true;

            while (hasMorePages)
            {
                var html = _EspnHttpClient.GetContent(nextUrl);

                var htmlDoc = new HtmlDocument();
                htmlDoc.OptionFixNestedTags = true;
                htmlDoc.LoadHtml(html);

                var rootNode = htmlDoc.DocumentNode;

                var result = ParsePlayers(rootNode, argTeamId);

                var navNode = rootNode.SelectSingleNode("//div[@class='paginationNav']");


                hasMorePages = false;

                if (navNode != null)
                {
                    var nextNode = navNode.ChildNodes.SingleOrDefault(e => e.InnerText.Contains("NEXT"));

                    if (nextNode != null)
                    {
                        nextUrl = nextNode.GetAttributeValue("href", "");
                        hasMorePages = true;
                    }
                }
            }

            return true;
        }


        private IEnumerable<int> MapPlayers(HtmlNode argRootNode, int argOwnerId)
        {
            //var playerRows = argRootNode.SelectNodes("//table[@id='playertable_0']/tbody/tr[@class='pncPlayerRow']/td[@class'playertablePlayerName']/a");
            var playerRows = argRootNode.SelectNodes("//td[@class='playertablePlayerName']/a");

            var playerIds = playerRows
                                .Select(e => e.Attributes["playerid"].Value)
                                .Select(e => int.Parse(e))
                                .ToList();

            return playerIds;
        }

        private List<Player> ParsePlayers(HtmlNode argRootNode, int argTeamId)
        {
            var result = new List<Player>();

            var playerTableNodes = argRootNode.SelectNodes("//div[@class='games-fullcol']/table");

            var positionRegex = new Regex("(QB|RB|WR|TE|D/ST|K)$");

            foreach (var tableNode in playerTableNodes)
            {
                var player = new Player();

                player.TeamId = argTeamId;

                var playerNode = tableNode.SelectSingleNode(".//tr[1]/td[1]/span/nobr/a");

                player.Name = playerNode.InnerHtml;
                player.EspnId = playerNode.GetAttributeValue("playerId", -1);

                var searchText = tableNode.SelectSingleNode(".//tr[1]/td[1]/span/nobr").InnerText;

                var match = positionRegex.Match(searchText);

                if (match.Success)
                {
                    player.Position = Enum.Parse<Enums.Position>(match.Value.Replace("/", ""));
                }

                var playerImageUrl = tableNode.SelectSingleNode(".//tr[2]/td[1]/img[1]").GetAttributeValue("src", "");
                var byteString = GetBase64StringFromUrl(playerImageUrl);

                player.ImageAsBase64 = $"data:image/png;base64,{byteString}";


                var imageId = Regex.Match(playerImageUrl, "/(\\d+).png").Groups[1].Value;
                if (int.TryParse(imageId, NumberStyles.Any, CultureInfo.InvariantCulture, out int imageIdParsed))
                { 
                    player.EspnImageNumber = imageIdParsed;
                }

                var projectedPointsNode = tableNode.SelectSingleNode(".//tr[3]/td[last()]").InnerText;

                if (double.TryParse(projectedPointsNode, NumberStyles.Any, CultureInfo.InvariantCulture, out var projectedPointsParsed))
                {
                    player.ProjectedPoints = projectedPointsParsed;
                }


                result.Add(player);

                _DataContext.Players.Add(player);
                _DataContext.SaveChanges();
            }
            
            return result;
        }

        //& //*[@id="content"]/div/div[4]/div/div/div[3]/div[3]/div/table[1]/tbody/tr[3]/td[11]

        private string GetBase64StringFromUrl(string argUrl)
        {
            try
            {
                var indexOfPng = argUrl.IndexOf("&background", StringComparison.InvariantCultureIgnoreCase);
                var url = argUrl.Substring(0, indexOfPng);

                using (WebClient wc = new WebClient())
                {
                    byte[] bytes = wc.DownloadData(url);
                    return Convert.ToBase64String(bytes);
                }
            }
            catch(Exception ex)
            {
                _Logger.Warning(ex, $"Could not get player image. baseUrl was <{argUrl}>");
                return "";
            }
        }
    }
}
