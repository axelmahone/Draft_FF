﻿namespace Draft_FF.Frontend.Service
{
    using Draft_FF.Frontend.DataAccess;
    using Serilog;

    public abstract class CDraftService
    {
        protected readonly ILogger Logger;
        protected readonly DataContext DataContext;


        protected CDraftService(ILogger argLogger, DataContext argDataContext)
        {
            Logger = argLogger;
            DataContext = argDataContext;
        }
    }
}
