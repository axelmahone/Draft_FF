﻿namespace Draft_FF.Frontend.Service
{
    using Draft_FF.Frontend.Entities;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class DraftBoard
    {
        private readonly Dictionary<int, DraftPickSlot> _OrderedDraftPickSlots;

        public DraftBoard(List<DraftPickSlot> argDraftPickSlot)
        {
            _OrderedDraftPickSlots = argDraftPickSlot.OrderBy(e => e.Overall).ToDictionary(e => e.Overall, e => e);
        }

        public int CurrentDraftPickKey
        {
            get
            {
                var firstSlotWithoutSelectedPlayer = _OrderedDraftPickSlots.FirstOrDefault(e => e.Value.SelectedPlayerId.HasValue);

                return (default(KeyValuePair<int, Draft>).Equals(firstSlotWithoutSelectedPlayer)) ? -1 : firstSlotWithoutSelectedPlayer.Key;
            }
        }

        public void Pick(Player argPlayer)
        {
            if (CurrentDraftPickKey >= 0)
            {
                _OrderedDraftPickSlots[CurrentDraftPickKey].SelectedPlayer = argPlayer;
            }
        }
    }
}
