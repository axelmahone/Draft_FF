﻿namespace Draft_FF.Frontend.Services
{
    using Draft_FF.Frontend.DataAccess;
    using Draft_FF.Frontend.Entities;
    using System.Collections.Generic;
    using System.Linq;

    public interface IUserService
    {
        Owner Authenticate(int argOwnerId);
        IEnumerable<Owner> GetAll();
        Owner GetById(int argOwnerId);
    }

    public class UserService : IUserService
    {
        private DataContext _Context;

        public UserService(DataContext argContext)
        {
            _Context = argContext;
        }

        public Owner Authenticate(int argOwnerId)
        {
            var user = _Context.Owners.SingleOrDefault(x => x.Id == argOwnerId);

            if (user == null)
                return null;
                 
            return user;
        }

        public IEnumerable<Owner> GetAll()
        {
            return _Context.Owners;
        }

        public Owner GetById(int argOwnerId)
        {
            return _Context.Owners.Find(argOwnerId);
        }
    }
}