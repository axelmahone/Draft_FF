﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Draft_FF.Frontend.Helpers;
using Draft_FF.Frontend.Entities;
using System.Net.Http;
using System.Net;
using Serilog;
using Draft_FF.Frontend.DTOs;
using Draft_FF.Frontend.Service;
using Draft_FF.Frontend.DataAccess;
using Microsoft.AspNetCore.Authorization;
using AutoMapper;

namespace Draft_FF.Frontend.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DraftController : ControllerBase
    {
        private readonly DataContext _Context;
        private readonly ILogger _Logger;
        private readonly DraftHub _DraftHub;
        private readonly IMapper _Mapper;

        public DraftController(DataContext context, ILogger argLogger, DraftHub argDraftHub, IMapper argMapper)
        {
            _Context = context;
            _Logger = argLogger;
            _DraftHub = argDraftHub;
            _Mapper = argMapper;
        }
        
        [HttpGet("DraftSlots")]
        public IEnumerable<DraftSlotDto> DraftSlots()
        {
            try
            {
                var draftSlots = _Context.DraftPickSlots;

                return _Mapper.Map<IList<DraftSlotDto>>(draftSlots);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex, $"{nameof(OwnerController)}.{nameof(DraftSlots)}() failed");

                return new List<DraftSlotDto>();
            }
        }

        [HttpPost("Pick")]
        public IActionResult Pick([FromBody] PickDto argPick)
        {
            try
            {
                var selectedPlayer = _Context.Players.Find(argPick.PlayerId);
                var currentPick = _Context
                    .DraftPickSlots
                    .OrderBy(e => e.Overall)
                    .FirstOrDefault(e => e.SelectedPlayerId.HasValue == false);

                currentPick.SelectedPlayer = selectedPlayer;

                var info = new DraftPickInfoDto();
                info.CurrentDraftSlot = _Mapper.Map<DraftSlotDto>(currentPick);
                info.PickedPlayer = _Mapper.Map<PlayerDto>(selectedPlayer);

                _DraftHub.NotifyOwners(info);
            }
            catch(Exception ex)
            {
                _Logger.Error(ex, $"Could not process pick.");
            }
            return NoContent();
        }


        [HttpPost("MapPlaceholders")]
        public IActionResult MapPlaceholders()
        {
            try
            {
                var slots = _Context.DraftPickSlots;

                foreach(var slot in slots)
                {
                    slot.OwnerId = slot.PlaceHolder + 1;
                }

                _Context.SaveChanges();
            }
            catch (Exception ex)
            {
                _Logger.Error(ex, $"MapPlaceholders failed");
            }

            return NoContent();

        }

        [HttpPost("CreateDraft")]
        public async Task<IActionResult> CreateDraft()
        {
            try
            {
                var draftPickSlots = new List<DraftPickSlot>();

                var numberOfTeams = 12;

                for (int placeholder = 0; placeholder < 10; placeholder++)
                {
                    for (int round = 1; round <= 12; round++)
                    {
                        var overall = numberOfTeams * (round - 1) + (placeholder + 1);

                        var slot = new DraftPickSlot() { Overall = overall, PlaceHolder = placeholder, Round = round, RankInRound = placeholder + 1 };

                        draftPickSlots.Add(slot);
                    }
                }

                // specialTeam [11]
                draftPickSlots.Add(new DraftPickSlot() { Overall = 11, PlaceHolder = 10, Round = 1, RankInRound = 11 });    //12
                draftPickSlots.Add(new DraftPickSlot() { Overall = 24, PlaceHolder = 10, Round = 2, RankInRound = 12 });    //24
                draftPickSlots.Add(new DraftPickSlot() { Overall = 35, PlaceHolder = 10, Round = 3, RankInRound = 11 });    //36
                draftPickSlots.Add(new DraftPickSlot() { Overall = 48, PlaceHolder = 10, Round = 4, RankInRound = 12 });    //48
                draftPickSlots.Add(new DraftPickSlot() { Overall = 59, PlaceHolder = 10, Round = 5, RankInRound = 11 });    //60
                draftPickSlots.Add(new DraftPickSlot() { Overall = 82, PlaceHolder = 10, Round = 6, RankInRound = 12 });    //72
                draftPickSlots.Add(new DraftPickSlot() { Overall = 73, PlaceHolder = 10, Round = 7, RankInRound = 11 });    //84
                draftPickSlots.Add(new DraftPickSlot() { Overall = 96,  PlaceHolder = 10, Round = 8, RankInRound = 12 });   //96
                draftPickSlots.Add(new DraftPickSlot() { Overall = 107, PlaceHolder = 10, Round = 9, RankInRound = 11 });   //108
                draftPickSlots.Add(new DraftPickSlot() { Overall = 120, PlaceHolder = 10, Round = 10, RankInRound = 12 });  //120
                draftPickSlots.Add(new DraftPickSlot() { Overall = 131, PlaceHolder = 10, Round = 11, RankInRound = 11 });  //132
                draftPickSlots.Add(new DraftPickSlot() { Overall = 144, PlaceHolder = 10, Round = 12, RankInRound = 12 });  //144

                // specialTeam [12]
                draftPickSlots.Add(new DraftPickSlot() { Overall = 12, PlaceHolder = 11, Round = 1, RankInRound = 12 });    //12
                draftPickSlots.Add(new DraftPickSlot() { Overall = 23, PlaceHolder = 11, Round = 2, RankInRound = 11 });    //24
                draftPickSlots.Add(new DraftPickSlot() { Overall = 36, PlaceHolder = 11, Round = 3, RankInRound = 12 });    //36
                draftPickSlots.Add(new DraftPickSlot() { Overall = 47, PlaceHolder = 11, Round = 4, RankInRound = 11 });    //48
                draftPickSlots.Add(new DraftPickSlot() { Overall = 60, PlaceHolder = 11, Round = 5, RankInRound = 12 });    //60
                draftPickSlots.Add(new DraftPickSlot() { Overall = 71, PlaceHolder = 11, Round = 6, RankInRound = 11 });    //72
                draftPickSlots.Add(new DraftPickSlot() { Overall = 84, PlaceHolder = 11, Round = 7, RankInRound = 12 });    //84
                draftPickSlots.Add(new DraftPickSlot() { Overall = 95, PlaceHolder = 11, Round = 8, RankInRound = 11 });    //96
                draftPickSlots.Add(new DraftPickSlot() { Overall = 108, PlaceHolder = 11, Round = 9, RankInRound = 12 });   //108
                draftPickSlots.Add(new DraftPickSlot() { Overall = 119, PlaceHolder = 11, Round = 10, RankInRound = 11 });  //120
                draftPickSlots.Add(new DraftPickSlot() { Overall = 132, PlaceHolder = 11, Round = 11, RankInRound = 12 });  //132
                draftPickSlots.Add(new DraftPickSlot() { Overall = 143, PlaceHolder = 11, Round = 12, RankInRound = 11 });  //144
                
                var placeholders = new int[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 10 };

                for(var round = 13; round <= 20; round++)
                {
                    for (int i = 0; i < placeholders.Length; i++)
                    {
                        var overall = placeholders.Length * (round - 1) + (i + 1);

                        var slot = new DraftPickSlot() { Overall = overall, PlaceHolder = placeholders[i], Round = round, RankInRound = i + 1 };

                        draftPickSlots.Add(slot);
                    }

                    Array.Reverse(placeholders);

                    if (round % 2 == 0)
                    {
                        Array.Reverse(placeholders, 10, 2);
                    }

                }

                var draft = new Draft() { DraftName = "Draft 2018", Slots = draftPickSlots };

                await _Context.Drafts.AddAsync(draft);
                await _Context.SaveChangesAsync();
            }
            catch(Exception ex)
            {
                _Logger.Error(ex, $"creating draft failed");
            }

            return NoContent();
        }
    }
}