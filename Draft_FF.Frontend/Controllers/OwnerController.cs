﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Draft_FF.Frontend.Helpers;
using Draft_FF.Frontend.DTOs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Draft_FF.Frontend.DataAccess;
using Microsoft.AspNetCore.Authorization;
using Draft_FF.Frontend.Services;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using AutoMapper;

namespace Draft_FF.Frontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OwnerController : ControllerBase
    {

        private readonly IUserService _UserService;
        private readonly DataContext _Context;
        private readonly ILogger _Logger;
        private readonly IMapper _Mapper;
        private readonly AppSettings _AppSettings;

        public OwnerController(DataContext argDataContext, ILogger argLogger, IUserService argUserService, IOptions<AppSettings> appSettings, IMapper argMapper)
        {
            _Context = argDataContext;
            _Logger = argLogger;
            _UserService = argUserService;
            _AppSettings = appSettings.Value;
            _Mapper = argMapper;
        }
        
        [HttpGet]
        public IEnumerable<OwnerDto> Get()
        {
            try
            {
                return _Mapper.Map<IList<OwnerDto>>(_Context.Owners);
            }
            catch(Exception ex)
            {
                _Logger.Error(ex, $"{nameof(OwnerController)}.{nameof(Get)}() failed");

                return new List<OwnerDto>();
            }
        }
        
        [HttpPost("authenticate/{argUserId}")]
        public IActionResult Authenticate([FromRoute]int argUserId)
        {
            var user = _UserService.Authenticate(argUserId);

            if (user == null)
                return BadRequest(new { message = "bad user" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_AppSettings.JwtSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            return Ok(new
            {
                Id = user.Id,
                Name = user.Name,
                ShortName = user.ShortName,
                TeamName = user.TeamName,
                Token = tokenString
            });
        }
    }
}