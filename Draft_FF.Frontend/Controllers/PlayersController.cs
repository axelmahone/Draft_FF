﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Draft_FF.Frontend.Entities;
using Serilog;
using Draft_FF.Frontend.Service;
using Draft_FF.Frontend.DataAccess;
using Draft_FF.Frontend.DTOs;
using AutoMapper;

namespace Draft_FF.Frontend.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PlayersController : ControllerBase
    {
        private readonly DataContext _Context;
        private readonly ILogger _Logger;
        private readonly IMapper _Mapper;

        public PlayersController(DataContext context, ILogger argLogger, IMapper argMapper)
        {
            _Context = context;
            _Logger = argLogger;
            _Mapper = argMapper;
        }

        [HttpGet("MapPlayers")]
        public IActionResult MapPlayers()
        {
            try
            {
                var importer = new PlayerImporter(_Context, _Logger);
                                
                var owners = _Context.Owners;

                foreach (var owner in owners)
                {
                    var imports = importer.ImportPlayerOwnerMapping(owner.Id);
                }
            }
            catch (Exception ex)
            {
                _Logger.Error(ex, $"ParseFailed");
            }

            return NoContent();
        }

        [HttpGet("ParseAndUpdateTeam/{argTeamId}")]
        public IActionResult ParseAndUpdateTeam([FromRoute] int argTeamId)
        {
            try
            {
                var importer = new PlayerImporter(_Context, _Logger);

                var teams = _Context.Teams.ToList();

                foreach (var team in teams)
                {
                    var imports = importer.ImportSingleTeam(team.Id);
                }
            }
            catch(Exception ex)
            {
                _Logger.Error(ex, $"ParseFailed");
            }
            
            return NoContent();
        }


        // GET: api/Players
        [HttpGet]
        public IEnumerable<PlayerDto> GetPlayers()
        {
            try
            {
                var players = _Context.Players.Include(e => e.Team);

                return _Mapper
                    .Map<IList<PlayerDto>>(players);
            }
            catch (Exception ex)
            {
                _Logger.Error(ex, $"{nameof(OwnerController)}.{nameof(GetPlayers)}() failed");

                return new List<PlayerDto>();
            }
        

    }

        // GET: api/Players/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetPlayer([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var player = await _Context.Players.FindAsync(id);

            if (player == null)
            {
                return NotFound();
            }

            return Ok(player);
        }

        // PUT: api/Players/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutPlayer([FromRoute] int id, [FromBody] Player player)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != player.Id)
            {
                return BadRequest();
            }

            _Context.Entry(player).State = EntityState.Modified;

            try
            {
                await _Context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!PlayerExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Players
        [HttpPost]
        public async Task<IActionResult> PostPlayer([FromBody] Player player)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _Context.Players.Add(player);
            await _Context.SaveChangesAsync();

            return CreatedAtAction("GetPlayer", new { id = player.Id }, player);
        }

        // DELETE: api/Players/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeletePlayer([FromRoute] Guid id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var player = await _Context.Players.FindAsync(id);
            if (player == null)
            {
                return NotFound();
            }

            _Context.Players.Remove(player);
            await _Context.SaveChangesAsync();

            return Ok(player);
        }

        private bool PlayerExists(int id)
        {
            return _Context.Players.Any(e => e.Id == id);
        }
    }
}