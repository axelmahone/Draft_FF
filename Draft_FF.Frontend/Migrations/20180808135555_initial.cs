﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Draft_FF.Frontend.Migrations
{
    public partial class initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Drafts",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    DraftName = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Drafts", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Owners",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    TeamName = table.Column<string>(maxLength: 50, nullable: false),
                    ShortName = table.Column<string>(maxLength: 3, nullable: false),
                    EspnId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Owners", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teams",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: false),
                    ShortName = table.Column<string>(nullable: false),
                    ByeWeek = table.Column<int>(nullable: false),
                    EspnId = table.Column<int>(nullable: false),
                    IsRealTeam = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teams", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Players",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: false),
                    Position = table.Column<string>(nullable: false),
                    ImageAsBase64 = table.Column<string>(nullable: true),
                    TeamId = table.Column<int>(nullable: false),
                    OwnerId = table.Column<int>(nullable: true),
                    EspnId = table.Column<int>(nullable: false),
                    EspnImageNumber = table.Column<int>(nullable: true),
                    ProjectedPoints = table.Column<double>(nullable: false),
                    Rank = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Players", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Players_Owners_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Owners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Players_Teams_TeamId",
                        column: x => x.TeamId,
                        principalTable: "Teams",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DraftPickSlots",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Overall = table.Column<int>(nullable: false),
                    Round = table.Column<int>(nullable: false),
                    RankInRound = table.Column<int>(nullable: false),
                    Notice = table.Column<string>(nullable: true),
                    PlaceHolder = table.Column<int>(nullable: false),
                    OwnerId = table.Column<int>(nullable: true),
                    SelectedPlayerId = table.Column<int>(nullable: true),
                    DraftId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DraftPickSlots", x => x.Id);
                    table.ForeignKey(
                        name: "FK_DraftPickSlots_Drafts_DraftId",
                        column: x => x.DraftId,
                        principalTable: "Drafts",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DraftPickSlots_Owners_OwnerId",
                        column: x => x.OwnerId,
                        principalTable: "Owners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_DraftPickSlots_Players_SelectedPlayerId",
                        column: x => x.SelectedPlayerId,
                        principalTable: "Players",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Teams",
                columns: new[] { "Id", "ByeWeek", "EspnId", "IsRealTeam", "Name", "ShortName" },
                values: new object[,]
                {
                    { 1, 0, 0, false, "FA", "FA" },
                    { 31, 9, 30, true, "Jacksonville Jaguars", "Jax" },
                    { 30, 4, 29, true, "Carolina Panthers", "Car" },
                    { 29, 4, 28, true, "Washington Redskins", "Wsh" },
                    { 28, 5, 27, true, "Tampa Bay Buccaneers", "TB" },
                    { 27, 7, 26, true, "Seattle Seahawks", "Sea" },
                    { 26, 11, 25, true, "San Francisco 49ers", "SF" },
                    { 25, 8, 24, true, "Los Angeles Chargers", "LAC" },
                    { 24, 7, 23, true, "Pittsburgh Steelers", "Pit" },
                    { 23, 9, 22, true, "Arizona Cardinals", "Ari" },
                    { 22, 9, 21, true, "Philadelphia Eagles", "Phi" },
                    { 21, 11, 20, true, "New York Jets", "NYJ" },
                    { 20, 9, 19, true, "New York Giants", "NYG" },
                    { 19, 6, 18, true, "New Orleans Saints", "NO" },
                    { 18, 11, 17, true, "New England Patriots", "NE" },
                    { 32, 10, 33, true, "Baltimore Ravens", "Bal" },
                    { 17, 10, 16, true, "Minnesota Vikings", "Min" },
                    { 15, 12, 14, true, "Los Angeles Rams", "LAR" },
                    { 14, 7, 13, true, "Oakland Raiders", "Oak" },
                    { 13, 12, 12, true, "Kansas City Chiefs", "KC" },
                    { 12, 9, 11, true, "Indianapolis Colts", "Ind" },
                    { 11, 8, 10, true, "Tennessee Titans", "Ten" },
                    { 10, 7, 9, true, "Green Bay Packers", "GB" },
                    { 9, 6, 8, true, "Detroit Lions", "Det" },
                    { 8, 10, 7, true, "Denver Broncos", "Den" },
                    { 7, 8, 6, true, "Dallas Cowboys", "Dal" },
                    { 6, 11, 5, true, "Cleveland Browns", "Cle" },
                    { 5, 9, 4, true, "Cincinnati Bengals", "Cin" },
                    { 4, 5, 3, true, "Chicago Bears", "Chi" },
                    { 3, 11, 2, true, "Buffalo Bills", "Buf" },
                    { 2, 8, 1, true, "Atlanta Falcons", "Atl" },
                    { 16, 11, 15, true, "Miami Dolphins", "Mia" },
                    { 33, 10, 34, true, "Houston Texans", "Hou" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_DraftPickSlots_DraftId",
                table: "DraftPickSlots",
                column: "DraftId");

            migrationBuilder.CreateIndex(
                name: "IX_DraftPickSlots_OwnerId",
                table: "DraftPickSlots",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_DraftPickSlots_SelectedPlayerId",
                table: "DraftPickSlots",
                column: "SelectedPlayerId",
                unique: true,
                filter: "[SelectedPlayerId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_Players_EspnId",
                table: "Players",
                column: "EspnId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Players_OwnerId",
                table: "Players",
                column: "OwnerId");

            migrationBuilder.CreateIndex(
                name: "IX_Players_TeamId",
                table: "Players",
                column: "TeamId");

            migrationBuilder.CreateIndex(
                name: "IX_Teams_EspnId",
                table: "Teams",
                column: "EspnId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Teams_Name",
                table: "Teams",
                column: "Name",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Teams_ShortName",
                table: "Teams",
                column: "ShortName",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DraftPickSlots");

            migrationBuilder.DropTable(
                name: "Drafts");

            migrationBuilder.DropTable(
                name: "Players");

            migrationBuilder.DropTable(
                name: "Owners");

            migrationBuilder.DropTable(
                name: "Teams");
        }
    }
}
