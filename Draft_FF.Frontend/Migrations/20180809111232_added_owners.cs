﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Draft_FF.Frontend.Migrations
{
    public partial class added_owners : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "ShortName",
                table: "Owners",
                maxLength: 4,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 3);

            migrationBuilder.InsertData(
                table: "Owners",
                columns: new[] { "Id", "EspnId", "Name", "ShortName", "TeamName" },
                values: new object[,]
                {
                    { 1, 1, "Simon W", "TMW", "Team Willi" },
                    { 2, 2, "Mathias Schmid", "LEAP", "Lambeau Leaper" },
                    { 3, 3, "Jakob Schallert", "WIN", "All I do is Winston" },
                    { 4, 4, "Valentin Moosmann", "STIG", "Stigling Ballers" },
                    { 5, 5, "Jonathan De Greve", "AfB", "Alvin and the KareemHunts" },
                    { 6, 6, "Dominik S", "AKH", "Team Willi" },
                    { 7, 7, "Daniel Puschnigg", "PUSC", "Team Puschnigg" },
                    { 8, 10, "Benjamin Puschnigg", "CM", "Hard CM" },
                    { 9, 11, "Luke Vlbg", "VLBG", "Team Vlbg" },
                    { 10, 12, "Buechele Juloan", "JULI", "SF 49er" },
                    { 11, 13, "Lukas G", "G", "Team G" },
                    { 12, 14, "Thomas Fink", "FINK", "Thomas Fink" }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Owners",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.AlterColumn<string>(
                name: "ShortName",
                table: "Owners",
                maxLength: 3,
                nullable: false,
                oldClrType: typeof(string),
                oldMaxLength: 4);
        }
    }
}
